---
layout: post
title: Eye, Brain and Vision - David Hubel 
---

David Hubel was a pioneering neurophysiologist who worked primarily on understanding the visual pathways in the brain. This blog-series will summaries all the key take-aways from his book - 'Eye, Brain and Vision'.
